fun main() {
    val text = listOf("admin", "set", "blue", "man", "how", "cool")

    println(text.slice(3 until text.size).joinToString(" "))
}