package events.vandal.eepresent.events

import com.destroystokyo.paper.event.block.BlockDestroyEvent
import events.vandal.eepresent.Main
import events.vandal.eepresent.commands.PresentCommand
import events.vandal.eepresent.utils.Colors
import events.vandal.eepresent.utils.Presents
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.TextColor
import org.bukkit.ChatColor
import org.bukkit.GameRule
import org.bukkit.NamespacedKey
import org.bukkit.Sound
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.EquipmentSlot

object PresentRightClickEvent : Listener {
    @EventHandler
    fun onPresentRightClick(ev: PlayerInteractEvent) {
        if (ev.action == Action.RIGHT_CLICK_BLOCK && Presents.presents.any { it.value.position.block == ev.clickedBlock } && ev.hand == EquipmentSlot.HAND && !ev.hasItem()) {
            ev.isCancelled = true
            val present = Presents.presents.filter { it.value.position.block == ev.clickedBlock }.values.first()

            if (present.acquired.contains(ev.player.uniqueId)) {
                ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}You have already acquired this present!")
                return
            }

            ev.player.playSound(ev.player.location, Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F)
            ev.player.sendMessage("${Colors.PREFIX}You have acquired the \"${PresentCommand.colorFromType(present.type)}${present.name}${ChatColor.RESET}\" present!")

            present.acquired.add(ev.player.uniqueId)

            if (PresentCommand.collectStats(ev.player).size == 1) {
                ev.player.getAdvancementProgress(Main.plugin.server.getAdvancement(NamespacedKey.fromString("eepresent/nft", Main.plugin)!!)!!).awardCriteria("0")
            }

            if (PresentCommand.collectStats(ev.player).size == Presents.presents.size) {
                ev.player.getAdvancementProgress(Main.plugin.server.getAdvancement(NamespacedKey.fromString("eepresent/all_presents", Main.plugin)!!)!!).awardCriteria("0")

                if (!ev.player.world.getGameRuleValue(GameRule.ANNOUNCE_ADVANCEMENTS)!!) {
                    Main.plugin.server.broadcast(Component.translatable("chat.type.advancement.challenge", ev.player.displayName(), Component.text("[How the Grinch Stole Christmas]").color(TextColor.color(170, 0, 170)).hoverEvent(Component.text("How the Grinch Stole Christmas\nCollect every present in the map.").color(TextColor.color(170, 0, 170)))))
                }
            }

            Presents.presents[present.id] = present

            Presents.save()
        }
    }

    @EventHandler
    fun onPresentBreak(ev: BlockBreakEvent) {
        if (Presents.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true

            ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}Don't break the present!")
        }
    }

    @EventHandler
    fun onPresentDestroy(ev: BlockDestroyEvent) {
        if (Presents.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true
        }
    }
}