package events.vandal.eepresent.utils

enum class PresentTypes(val value: Int) {
    GREEN(0),
    CYAN(1),
    GOLD(2),
    GREEN2(3),
    BLUE(4),
    RED(5);

    companion object {
        fun fromInt(value: Int) = PresentTypes.values().first { it.value == value }
    }
}