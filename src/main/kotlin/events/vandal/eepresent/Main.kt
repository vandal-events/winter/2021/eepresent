package events.vandal.eepresent

import events.vandal.eepresent.commands.PresentCommand
import events.vandal.eepresent.events.PresentRightClickEvent
import events.vandal.eepresent.utils.Presents
import hu.trigary.advancementcreator.Advancement
import hu.trigary.advancementcreator.AdvancementFactory
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.Particle
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class Main : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        if (!this.dataFolder.exists())
            this.dataFolder.mkdir()

        this.getCommand("present")?.setExecutor(PresentCommand)
        this.getCommand("present")?.tabCompleter = PresentCommand

        this.server.pluginManager.registerEvents(PresentRightClickEvent, this)
        //this.server.pluginManager.registerEvents(PlayerAntiDamageEvent, this)

        particleTask = this.server.scheduler.runTaskTimer(this, Runnable {
            Presents.presents.forEach {
                this.server.onlinePlayers.forEach {itt ->
                    if (!it.value.acquired.contains(itt.uniqueId)) {
                        val loc = Location(it.value.world, it.value.position.blockX + 0.5, it.value.position.blockY + 0.6, it.value.position.blockZ + 0.5)
                        itt.spawnParticle(Particle.VILLAGER_HAPPY, loc, 10)
                    }
                }
            }
        }, 50, 45)

        val factory = AdvancementFactory(this, false, false)

        val root = factory.getRoot("eepresent/root", "Presents", "Time to collect everything!", Material.SHULKER_BOX, "block/dark_oak_trapdoor")
        root.activate(false)

        val nftsAreBad = factory.getImpossible("eepresent/nft", root, "Evil Right Clicker", "Collect your first present.", Material.PLAYER_HEAD).setFrame(Advancement.Frame.TASK).setHidden(true)

        val allPresents = factory.getImpossible("eepresent/all_presents", nftsAreBad, "How the Grinch Stole Christmas", "Collect every present in the map.", Material.CREEPER_HEAD).setFrame(Advancement.Frame.CHALLENGE).setHidden(true)
        nftsAreBad.activate(false)
        allPresents.activate(false)

        Bukkit.reloadData()
    }

    override fun onDisable() {
        this.server.scheduler.cancelTask(particleTask.taskId)
        Presents.save()
    }

    companion object {
        lateinit var plugin: JavaPlugin
        private lateinit var particleTask: BukkitTask
    }
}